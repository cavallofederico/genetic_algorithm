import math

from nsga2 import seq
from nsga2.problems.problem_definitions import ProblemDefinitions

class ScanningDefinitions(ProblemDefinitions):

    def __init__(self):
        self.n = 22
        self.PRESPONSE_I = 0
        self.OP_CHANNEL = 6
        self.NIC_CHANNEL = 7
        self.DELAY_J = 8
        self.DELAY_K = 9
        self.MAC = 14
        self.TIMER_MIN = 0
        self.TIMER_MAX = 1
        self.LATENCY = 2
        self.DISCOVERY_RATE = 0
        self.FAILED = 1

    def fitness_scanning(self, features, broadcast_fitness_table):
        fitness_table = broadcast_fitness_table        
        channels = [[0,0]]
        k=0
        while k < len(features):
            timer_min = features[k]
            timer_max = features[k+1]
            channels.append([timer_min, timer_max])
            k += 2

        results_discovery_rate = []
        results_failed = []
        results_latency = []
        APs_in_trace = set()
        APs_found_in_trace = set()
        #Triying different discovery rate for new results
        APs_found_in_trace_for_other_discovery_rate = set()
        Total_existing_APs = set()
        #
        failed = 1
        i = 1
        latency_vector = []
        for channel in channels:
            latency_vector.append(channel[self.TIMER_MIN])
        while i < len(fitness_table):

            line = fitness_table[i]
            APs_in_trace.add(line[self.MAC])
            #Triying different discovery rate for new results
            Total_existing_APs.add(line[self.MAC])
            #
            if channels[line[self.NIC_CHANNEL]][self.TIMER_MIN] >= line[self.DELAY_K]:
                APs_found_in_trace.add(line[self.MAC])
                #Triying different discovery rate for new results
                APs_found_in_trace_for_other_discovery_rate.add(line[self.MAC])
                #
                latency_vector[line[self.NIC_CHANNEL]] = channels[line[self.NIC_CHANNEL]][self.TIMER_MIN] + channels[line[self.NIC_CHANNEL]][self.TIMER_MAX]
            elif latency_vector[line[self.NIC_CHANNEL]] >= line[self.DELAY_K]:
                APs_found_in_trace.add(line[self.MAC])
                #Triying different discovery rate for new results
                APs_found_in_trace_for_other_discovery_rate.add(line[self.MAC])
                #

            if line[self.PRESPONSE_I] == 1 and line[self.NIC_CHANNEL] <= fitness_table[i-1][self.NIC_CHANNEL]:
                number_APs_in_trace = len(APs_in_trace)
                number_APs_found_in_trace = len(APs_found_in_trace)
                if number_APs_found_in_trace > 0:
                    failed = 0
                if number_APs_in_trace > 0:
                    discovery_rate = float(number_APs_found_in_trace) / float(number_APs_in_trace)
                else:
                    discovery_rate = 0
                latency = sum(latency_vector)
                results_discovery_rate.append(discovery_rate)
                results_failed.append(failed)
                results_latency.append(latency)

                APs_in_trace = set()
                APs_found_in_trace = set()
                latency_vector = []
                for channel in channels:
                    latency_vector.append(channel[self.TIMER_MIN])
        
                failed = 1
            i += 1
        avg_latency = float(sum(results_latency)) / float(len(results_latency))
        avg_discovery_rate = sum(results_discovery_rate) / len(results_discovery_rate)
        failure_rate = float(sum(results_failed)) / float(len(results_failed))
        #Triying different discovery rate for new results
        other_discovery_rate = float(len(APs_found_in_trace_for_other_discovery_rate))/float(len(Total_existing_APs))

        #Triying different discovery rate for new results        
#        return [discovery_rate, failure_rate, avg_latency]
        return [other_discovery_rate, failure_rate, avg_latency]
#

