"""NSGA-II related functions"""

import functools
from nsga2.population import Population
import random
import numpy as np
from nsga2.individual import Individual
from nsga2.problems.scanning import Scanning
import itertools
from itertools import chain, imap

class NSGA2Utils(object):
    
    def __init__(self, problem, num_of_individuals, mutation_strength=10, num_of_genes_to_mutate=5, num_of_tour_particips=2):
        self.FEATURES = 0
        self.OBJECTIVES = 1
        self.DOMINATION_COUNT = 2
        self.DOMINATED_SOLUTIONS = 3
        self.RANK = 4
        self.CROWDING_DISTANCE = 5
        self.OBJECTIVE1 = 0
        self.OBJECTIVE2 = 1
        self.OBJECTIVE3 = 2
        self.HASH = 6
        
        self.problem = problem
        self.num_of_individuals = num_of_individuals
        self.mutation_strength = mutation_strength
        self.number_of_genes_to_mutate = num_of_genes_to_mutate
        self.num_of_tour_particips = num_of_tour_particips
        #self.mutation_type = mutation_type
        
    def __take_out_objective(self, individual_S, objective):
        objectives_S = individual_S[self.OBJECTIVES][objective]
        return objectives_S
	
                        
    def fast_nondominated_sort(self, population):
        population.fronts = []
        population.fronts.append([])
        self.problem.define_min_maxS2([None,None,None],[None,None,None])

        for individual in population:
            if individual[self.HASH] == 0:
                continue
            self.problem.define_min_maxS([individual[self.OBJECTIVES][0], individual[self.OBJECTIVES][1], individual[self.OBJECTIVES][2]], [individual[self.OBJECTIVES][0], individual[self.OBJECTIVES][1], individual[self.OBJECTIVES][2]])
            #print individual[self.HASH]
            individual[self.DOMINATION_COUNT] = 0
            individual[self.DOMINATED_SOLUTIONS] = []
            for other_individual in population:
                if (individual <> other_individual and individual[self.HASH] == other_individual[self.HASH]) or other_individual[self.HASH] == 0:
                    other_individual[self.HASH] = 0
                    continue
                        

                if self.dominatesS_notEval(other_individual[self.OBJECTIVES], individual[self.OBJECTIVES]):                   
                    individual[self.DOMINATED_SOLUTIONS].append(other_individual)
                elif self.dominatesS_notEval(individual[self.OBJECTIVES], other_individual[self.OBJECTIVES]):
                    individual[self.DOMINATION_COUNT] += 1
            if individual[self.DOMINATION_COUNT] == 0: 
                population.fronts[0].append(individual)
                individual[self.RANK] = 0 
        i = 0
        while len(population.fronts[i]) > 0:
            temp = []
            for individual in population.fronts[i]:
                if individual[self.HASH] == 0:
                    continue
                for other_individual in individual[self.DOMINATED_SOLUTIONS]:
                    if other_individual[self.HASH] == 0:
                        continue
                    other_individual[self.DOMINATION_COUNT] -= 1
                    if other_individual[self.DOMINATION_COUNT] == 0:
                        other_individual[self.RANK] = i+1
                        temp.append(other_individual)
                    
            population.fronts.append(temp)
            i = i+1


    def __sort_objective(self, val1, val2, m):
        return cmp(val1[self.OBJECTIVES][m], val2[self.OBJECTIVES][m])
    
    def calculate_crowding_distance(self, front):
        if len(front) > 0:
            solutions_num = len(front)
            for individual in front:
                individual[self.CROWDING_DISTANCE] = 0
            
            for m in range(len(front[0][self.OBJECTIVES])):
                front = sorted(front, cmp=functools.partial(self.__sort_objective, m=m))
                front[0][self.CROWDING_DISTANCE] = self.problem.max_objectives[m]
                front[solutions_num-1][self.CROWDING_DISTANCE] = self.problem.max_objectives[m]
                for index, value in enumerate(front[1:solutions_num-1]):
                    if (self.problem.max_objectives[m] - self.problem.min_objectives[m]) <> 0:
                        front[index][self.CROWDING_DISTANCE] = (front[index+1][self.CROWDING_DISTANCE] - front[index-1][self.CROWDING_DISTANCE]) / (self.problem.max_objectives[m] - self.problem.min_objectives[m])
                    else:
                        front[index][self.CROWDING_DISTANCE] = (front[index+1][self.CROWDING_DISTANCE] - front[index-1][self.CROWDING_DISTANCE]) / 0.01
           
    def crowding_operator(self, individual, other_individual):
        if (individual[self.RANK] < other_individual[self.RANK]) or \
            ((individual[self.RANK] == other_individual[self.RANK]) and (individual[self.CROWDING_DISTANCE] > other_individual[self.CROWDING_DISTANCE])):
            return 1
        else:
            return -1
    
    def create_initial_population(self,broadcast_fitness_table):
        population = Population()
        #population_spark = sc.parallelize()
        #for _ in range(self.num_of_individuals):
            #individual = self.problem.generateIndividualSpark(broadcast_fitness_table)
            #population.population.append(individual)
        population.population = map(lambda x: self.problem.generateIndividualSpark(broadcast_fitness_table),range(self.num_of_individuals))
        return population
    
    def create_children(self, population, broadcast_fitness_table):
        #children_S = []
        #for _ in range(len(population)/2):
            #childs = self.generate_children_Spark(population, broadcast_fitness_table)
            #children_S.extend(childs)
        children_S = self.flatmap(lambda x: self.generate_children_Spark(population, broadcast_fitness_table),range(len(population)/2))
        #children_S = [item for sublist in childs for item in sublist]
        
        #print "ok"
        
        return children_S

    def generate_children_Spark(self, population,broadcast_fitness_table):
        parent1 = self.__tournament(population)
        parent2 = parent1
        while parent1[self.FEATURES] == parent2[self.FEATURES]:

            parent2 = self.__tournament(population)
        child1, child2 = self.__crossover(parent1, parent2,broadcast_fitness_table)
        self.__mutate(child1)
        self.__mutate(child2)
        child1[self.OBJECTIVES] = self.problem.calculate_objectives_Spark(child1[self.FEATURES],broadcast_fitness_table)
        child2[self.OBJECTIVES] = self.problem.calculate_objectives_Spark(child2[self.FEATURES],broadcast_fitness_table)
        hash_value1 =  self.problem.calculate_hash(child1[self.FEATURES])
        hash_value2 =  self.problem.calculate_hash(child2[self.FEATURES])
        return [[child1[self.FEATURES], child1[self.OBJECTIVES], 0, [], 0, 0, hash_value1] , [child2[self.FEATURES], child2[self.OBJECTIVES], 0, [], 0, 0, hash_value2]]

    def __crossover(self, individual1, individual2,broadcast_fitness_table):
        child1 = self.problem.generateIndividualSpark(broadcast_fitness_table)
        child2 = self.problem.generateIndividualSpark(broadcast_fitness_table)
        genes_indexes = range(len(child1[self.FEATURES]))
        random.seed()
        half_genes_indexes = random.sample(genes_indexes, int(len(child1[self.FEATURES])/2))
        for i in genes_indexes:
            if i in half_genes_indexes:
                child1[self.FEATURES][i] = individual2[self.FEATURES][i]
                child2[self.FEATURES][i] = individual1[self.FEATURES][i]
            else:
                child1[self.FEATURES][i] = individual1[self.FEATURES][i]
                child2[self.FEATURES][i] = individual2[self.FEATURES][i]
        return child1, child2

    def __mutate(self, child):
#TODO change uniform for gaussian in mutation?
        #mutation_type: could be discrete 0, or...
        genes_to_mutate = random.sample(range(0, len(child[self.FEATURES])), self.number_of_genes_to_mutate)
        random.seed()
        for gene in genes_to_mutate:
            #child[self.FEATURES][gene] = int(child[self.FEATURES][gene] - self.mutation_strength/2 + random.random() * self.mutation_strength)
            child[self.FEATURES][gene] = child[self.FEATURES][gene] + random.randint(-self.mutation_strength,self.mutation_strength)
            if child[self.FEATURES][gene] < 0:
                child[self.FEATURES][gene] = 0
            elif child[self.FEATURES][gene] > 250:
                child[self.FEATURES][gene] = 250
        
    def __tournament(self, population):
        participants = random.sample(population, self.num_of_tour_particips)
        best = None
        for participant in participants:
            if best is None or self.crowding_operator(participant, best) == 1:
                best = participant

        return best
    def compute_max_min(self,population):
        
        take_out_objective1 = functools.partial(self.__take_out_objective, objective=self.OBJECTIVE1)
        take_out_objective2 = functools.partial(self.__take_out_objective, objective=self.OBJECTIVE2)
        take_out_objective3 = functools.partial(self.__take_out_objective, objective=self.OBJECTIVE3)

        minS = [0,0,0]
        maxS = [0,0,0]

        minS[self.OBJECTIVE1], minS[self.OBJECTIVE2],minS[self.OBJECTIVE3] = min(map(take_out_objective1,population)), min(map(take_out_objective2,population)), min(map(take_out_objective3,population))
        maxS[self.OBJECTIVE1], maxS[self.OBJECTIVE2], maxS[self.OBJECTIVE3] = max(map(take_out_objective1,population)), max(map(take_out_objective2,population)), max(map(take_out_objective3,population))
        self.problem.define_min_maxS2(minS, maxS)


    def dominatesS_notEval(self, objectives2, objectives1):

        worse_than_other = objectives1[0] >= objectives2[0] and objectives1[1] <= objectives2[1] and objectives1[2] <= objectives2[2]
        better_than_other = objectives1[0] > objectives2[0] or objectives1[1] < objectives2[1] or objectives1[2] < objectives2[2]
        return worse_than_other and better_than_other

    def flatmap(self,f, items):
        return chain.from_iterable(imap(f, items))
