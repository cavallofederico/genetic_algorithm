#from metrics.problems.scanning import ScanningMetrics

import sys

from nsga2.evolution import Evolution
from nsga2.problems.scanning import Scanning
from nsga2.problems.scanning.scanning_definitions import ScanningDefinitions
import cProfile
import pstats
from nsga2.plotter import Plotter
#from pyspark import SparkContext, SparkConf

def print_generation(population, generation_num):
    print("Generation: {}".format(generation_num))

def print_metrics(population, generation_num):
    pareto_front = population.fronts[0]
    metrics = ScanningMetrics()
    hv = metrics.HV(pareto_front)
    hvr = metrics.HVR(pareto_front)
    print("HV: {}".format(hv))
    print("HVR: {}".format(hvr))

collected_metrics = {}
def collect_metrics(population, generation_num):
    pareto_front = population.fronts[0]
    metrics = ScanningMetrics()
    hv = metrics.HV(pareto_front)
    hvr = metrics.HVR(pareto_front)
    collected_metrics[generation_num] = hv, hvr

def quiet_logs( sc ):
  logger = sc._jvm.org.apache.log4j
  logger.LogManager.getLogger("org"). setLevel( logger.Level.ERROR )
  logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )

#sc = SparkContext(appName="NSGA2_Spark")
#quiet_logs(sc)

output_plots = sys.argv[1]
scanning_definitions = ScanningDefinitions()
plotter = Plotter(scanning_definitions, output_plots)
problem = Scanning(scanning_definitions, 22) # number of genes

evolution = Evolution(problem, 20, 20) # generations, individuals, number of genes
evolution.register_on_new_generation(plotter.plot_population_best_front)
evolution.register_on_new_generation(print_generation)
#evolution.register_on_new_generation(print_metrics)
#evolution.register_on_new_generation(collect_metrics)
cProfile.run('pareto_front = evolution.evolve()','restats')
p = pstats.Stats('restats')
p.sort_stats('time').print_stats(20)
#pareto_front = evolution.evolve()

#plotter.plot_x_y(collected_metrics.keys(), map(lambda (hv, hvr): hvr, collected_metrics.values()), 'generation', 'HVR', 'HVR metric for ZDT3 problem', 'hvr-zdt3')

sc.stop()
